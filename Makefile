SHELL := /bin/bash
.DEFAULT_GOAL := help

# includes
-include makefiles/*

# Path variables
MAKEFILE_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
GIT_ROOT := $(shell git rev-parse --show-toplevel)
GALAXY_FILE = $(shell find $(GIT_ROOT) -regex '.*/galaxy.y?ml')

# Text colors
yellow = $(shell tput setaf 3)
magenta = $(shell tput setaf 5)
reset = $(shell tput sgr0)

# -----------------------------------------------------------------------------
# TARGETS
# -----------------------------------------------------------------------------

.PHONY: help
help: ## Displays all available targets with their descriptions
	@awk 'BEGIN { FS = ":.*##" } \
        /^[a-zA-Z_-]+:.*?##/ { \
          printf "$(yellow)%-25s$(reset) $(magenta)%s$(reset)\n", $$1, $$2 \
        }' $(MAKEFILE_LIST) | sort

.PHONY: install-pipenv
install-pipenv: ## Installs the virtual env
	cd $(MAKEFILE_DIR) && pipenv install

.PHONY: setup-env
setup-env: install-pipenv ## Installs the virtual env and pre-commit
	cd $(MAKEFILE_DIR) && \
		pipenv install --dev && \
		pipenv run pre-commit install && \
		pipenv run pre-commit autoupdate

.PHONY: create-collection
create-collection: install-pipenv ## Create collection
	@test $(version) || { echo "ERROR: version is required"; exit 1; }
	pipenv run ansible-playbook \
		--extra-vars @$(GALAXY_FILE) \
		--extra-vars version=$(version) \
		./playbook.yml

.PHONY: publish-collection
publish-collection: install-pipenv ## Publish collection to ansbile galaxy
	@test $(version) || { echo "ERROR: version is required"; exit 1; }
	pipenv run ansible-playbook \
		--extra-vars @$(GALAXY_FILE) \
		--extra-vars version=$(version) \
		--tags all,publish \
		./playbook.yml
