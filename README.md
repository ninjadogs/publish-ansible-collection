# Publish Ansible Collection

This playbook will build a collection, publish it to ansible galaxy and create and push git tags.
To ensure that only one README file needs to be maintained, the collection must be located at the top level of the repository.
The collection repo structure should be:

```
- git root dir/
    - galaxy.yml
    - roles/
    - plugins/
    - ...
    - publish/
        - playbook.yml
        - Makefile
```

To publish a collection use the make target `publish-collection` and pass a `version` to it:

```
make version=1.0.0 publish-collection
```

## Required Environment Variables

- `ANSIBLE_GALAXY_TOKEN`
